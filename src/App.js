import React, { Component, Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css'
import Home from './components/Home';
import Footer from './components/partials/Footer';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AboutUs from './components/AboutUs';
import PrivacyPolicy from './components/PrivacyPolicy';
import RefundPolicy from './components/RefundPolicy';
import TermsConditions from './components/TermsConditions';



 
class App extends Component{
  constructor(props){
    super(props); 
  }
  
 

  render(){
    return(
      <Fragment>
        <Router>
          <Switch>
            <Route exact path="/">
                <Home />      
            </Route>
            <Route exact path="/about-us">
              <AboutUs />
            </Route>
            <Route exact path="/privacy-policy">
              <PrivacyPolicy />
            </Route>
            <Route exact path="/refund-policy">
              <RefundPolicy />
            </Route>
            <Route exact path="/terms-and-conditions">
              <TermsConditions />
            </Route>
          </Switch>
        </Router>
      </Fragment>
    )
  }
}
 



export default App;