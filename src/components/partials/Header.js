import React, { Component, Fragment } from 'react';
import downArrow from './../../assets/images/downArrow.png';
import sliderright from './../../assets/images/sliderright.png';
import headerblueplusmedium from './../../assets/images/headerblueplusmedium.png';
// import headerplay from './../../assets/images/headerplay.png';
import whitePlus from './../../assets/images/whitePlus.png';
import lightblue from './../../assets/images/lightblue.png';
import doclogo from './../../assets/images/doclogo.png';


 
class Header extends Component{


      

  componentDidMount(){
    window.addEventListener('scroll', () => {
       let activeClass = 'normal';
       if(window.scrollY === 0){
           activeClass = 'top';
       }
       this.setState({ activeClass });
    });
   


}   

    render(){ 
        return( 
            <Fragment>
                <header> 
                {/* Desktop Header */}
                    <div className="desktop-header">                    
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-6"></div>
                                <div className="col-md-6">
                                    <div className="slider-img">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#videoModal">
                                            <img src={ sliderright } className="img-fluid" />
                                            {/* <div className="header-play">
                                                <img src={headerplay} className="img-fluid" />
                                            </div> */}
                                        </a>
                                        <div className="blue-small-icon">
                                            <img src= {headerblueplusmedium} className="img-fluid" />
                                        </div>
                                        <div className="white-small-icon">
                                            <img src= {whitePlus} className="img-fluid" />
                                        </div>
                                        <div className="white-medium-icon">
                                            <img src= {whitePlus} className="img-fluid" />
                                        </div>
                                        <div className="blue-medium-icon">
                                            <img src= {headerblueplusmedium} className="img-fluid" />
                                        </div>
                                        <div className = "light-blue-medium-icon">
                                            <img src = {lightblue} className = "img-fluid" />
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    {/* Desktop Header */}
                    {/* Mobile Header */}
                    <div className="header-mobile">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-12 mobile-show-responsive">
                                    <div class="mob-slider-img">
                                        <img src= {doclogo} className = "img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Mobile Header */}
                     {/* ===================
                        *** Slider Section ***
                    ==========================
                     */}
                        <section className="slider-section">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="banner-text-area">
                                            <div className="banner-text-sec">
                                                <h4>A messaging platform which <span className="d-block">connects doctors to their patients</span></h4>    
                                            </div>
                                            <div className="slider-btns">
                                                <span className="d-inline-block" data-menuanchor="thirdSection" >
                                                    <a href="#thirdSection" className="common-btn light-blue-btn">Download</a>
                                                </span>
                                                <span className="d-inline-block ml-3" data-menuanchor="fifthSection">
                                                    <a href="#fifthSection" className="common-btn  dark-blue-btn abc" id="head_btn">Connect</a>
                                                </span>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="down-arrow" data-menuanchor="secondSection">
                                <a href="#secondSection">
                                    <img src={ downArrow } />
                                </a>
                            </div> 
                        </section>
                     {/* ===================
                        *** End Slider Section ***
                    ==========================
                     */} 
                </header>
        
            </Fragment>
        )
    }
}

export default Header;