import React, { Component, Fragment } from 'react';
import OwlCarousel from 'react-owl-carousel2';
// import 'react-owl-carousel2/style.css'; 
//Allows for server-side rendering.
import iosPlayStore from './../../assets/images/iosPlayStore.png';
import googlePlayStore from './../../assets/images/googlePlayStore.png';
import bluephone from './../../assets/images/bluephone.png';
import whitephone from './../../assets/images/whitephone.png';
import whitePlus from './../../assets/images/whitePlus.png';
import CS1 from './../../assets/images/CS1.png';
import doctor from './../../assets/images/doctor.gif';
import patient from './../../assets/images/patient.gif';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
 
class DownloadApp extends Component{
    constructor(props){
        super(props);
        this.state = {
            isActive : 'doctor'
        }
    }
    options = {
        items: 1,
        nav: true, 
        autoplay: true
    };
    events = {
        onDragged: function(event) {

        },
        onChanged: function(event) {

        }
    };


    // Change Tabs Functions

    changeTabs = (event) => {
        let get_id = event.target.attributes.getNamedItem('data-id').value; 
        this.setState({
            isActive : get_id
        })
    }

    render(){
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            nav:false,
            arrows:false,
          };
        return(
            <Fragment>

                <section className="download-app desktop-view" id="boom">  
                    <div className="container-fluid"> 
                        <div className="row">
                            <div className="col-md-6 ios-side side">
                                <div className="donwload-apps-inner d-flex">
                                    <div className="app-text-icons"> 
                                        {/* <Slider {...settings}> */}  
                                            <div>
                                                <div className="app-text">
                                                    <h4>Doctors’ App</h4>
                                                    <p>Ease of use, work-life balance, additional revenue stream and protects your privacy</p>
                                                </div>
                                            </div>  
                                            {/* </Slider> */}
                                            <div className="apps-icons">
                                                <a href="https://play.google.com/store/apps/details?id=com.doclink.doctor" className="d-block">
                                                    <img src={ googlePlayStore }  className="img-fluid" />
                                                </a>
                                                <a href="https://apps.apple.com/us/app/doclink-doctor-app/id1510926246" className="d-block">
                                                    <img src={ iosPlayStore }  className="img-fluid" />
                                                </a>
                                            </div>   
                                        </div>
                                    <div className="phone-img">
                                        <img src={ doctor }  className="img-fluid" /> 
                                    </div>
                                </div>
                                <div className="review-plus-blue">
                                    <img src={ CS1 } />
                                </div>
                                <div className="review-plus-blue-sm">
                                    <img src={ CS1 } />
                                </div>
                            </div>

                            <div className="col-md-6 android-side side">
                            <div className="ttt">
                                <div className="donwload-apps-inner d-flex">
                                    <div className="phone-img">
                                        <img src={ patient }  className="img-fluid" />
                                    </div>
                                    <div className="app-text-icons"> 
                                        {/* <Slider {...settings}> */}
                                            <div>
                                                <div className="app-text">
                                                    <h4>Patients’ App</h4>
                                                    <p>Well-being of patient, increased accessibility, secured data storage, no prior booking required and eliminates queues.</p>
                                                </div>
                                            </div> 
                                        {/* </Slider> */}
                                        <div className="apps-icons">
                                            <a href="https://apps.apple.com/us/app/doclink-patient-app/id1510926079" className="d-block">
                                                <img src={ googlePlayStore }  className="img-fluid" />
                                            </a>
                                            <a href="https://play.google.com/store/apps/details?id=com.doclink.patient" className="d-block">
                                                <img src={ iosPlayStore }  className="img-fluid" />
                                            </a>
                                        </div> 
                                    </div>
                                </div>
                                <div className="review-plus-white">
                                    <img src={ whitePlus } />
                                </div>
                                <div className="review-plus-white-sm">
                                    <img src={ whitePlus } />
                                </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </section>

                   <section className={ this.state.isActive == "doctor" ? "downapp-mobile-view white-bg" : "downapp-mobile-view blue-bg" }>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="our-review-heading">
                                    <p>Download Apps</p>
                                </div>
                            </div>    
                            <div className="col-md-12">
                                <div className="main-tabs">
                                    <div className="tabs-header">
                                        <ul className="no-default">
                                            <li className={ this.state.isActive == "doctor" ? "active" : "" } data-id="doctor" onClick = { this.changeTabs } > Doctors </li>
                                            <li className={ this.state.isActive == "patient" ? "active" : "" } data-id="patient" onClick = { this.changeTabs } > Patients </li>
                                        </ul>
                                    </div>
                                    <div className="tabs-body">
                                        <div className="tabs-inner">
                                            {
                                                this.state.isActive == "doctor" ? 
                                                   //Doctor 
                                                    <div className="download-gifs doctor-side">
                                                        <div className="gifs-section">
                                                            <div className="phone-img">
                                                                <img src={ doctor }  className="img-fluid" /> 
                                                                <p>Ease of use, work-life balance, additional revenue stream and protects your privacy.</p>
                                                            </div>
                                                            <div className="apps-icons">
                                                                <a href="https://play.google.com/store/apps/details?id=com.doclink.doctor" className="d-block">
                                                                    <img src={ googlePlayStore }  className="img-fluid" />
                                                                </a>
                                                                <a href="https://apps.apple.com/us/app/doclink-doctor-app/id1510926246" className="d-block">
                                                                    <img src={ iosPlayStore }  className="img-fluid" />
                                                                </a>
                                                            </div> 
                                                        </div>  
                                                    </div>  
                                                //END DOCTOR TAB's
                                            
                                                : 
                                                //Patient TAB's
                                                      //Doctor 
                                                    <div className="download-gifs patient-side">
                                                        <div className="gifs-section">
                                                            <div className="phone-img">
                                                                <img src={ doctor }  className="img-fluid" /> 
                                                                <p>Well-being of patient, increased accessibility, secured data storage, no prior booking required and eliminates queues.</p>
                                                            </div>
                                                            <div className="apps-icons">
                                                                <a href="https://play.google.com/store/apps/details?id=com.doclink.doctor" className="d-block">
                                                                    <img src={ googlePlayStore }  className="img-fluid" />
                                                                </a>
                                                                <a href="https://apps.apple.com/us/app/doclink-doctor-app/id1510926246" className="d-block">
                                                                    <img src={ iosPlayStore }  className="img-fluid" />
                                                                </a>
                                                            </div> 
                                                        </div>  
                                                    </div>  
                                                //END DOCTOR TAB's
                                                //End Patient TAB's 
                                            }
                                            
                                            

                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                   
                </section>
            </Fragment>
        )
    }
}


export default DownloadApp;