import React, { Component, Fragment } from 'react';
// import d01 from './../../assets/images/d01.jpeg';
// import d02 from './../../assets/images/d02.jpg';
import DrTalaihaChughtai from './../../assets/images/DrTalaihaChughtai.png';
import DrSamiaHussain from './../../assets/images/DrSamiaHussain.png';
import DrKamranIqbal from './../../assets/images/DrKamranIqbal.png';
import patient1 from './../../assets/images/patient1.png';
import patient2 from './../../assets/images/patient2.png';
import patient3 from './../../assets/images/patient3.png';
import whitePlus from './../../assets/images/whitePlus.png';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

class Reviews extends Component{
    constructor(props){
        super(props);
        this.state = {
            isActive : 'doctor'
        }
    }


    // Change Tabs Functions

    changeTabs = (event) => {
        let get_id = event.target.attributes.getNamedItem('data-id').value; 
        this.setState({
            isActive : get_id
        })
    }

    render(){
        const settings = {
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            nav:true,
            arrows:true,
            className: "center",
            autoplay: true,
            autoplaySpeed: 6000,
            // centerMode: true,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    arrows:false,
                    nav:true,
                    dots:true
                  }
                },
                {
                    breakpoint: 520,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      nav:true,
                      dots:true,
                      arrows:true
                    }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    nav:true,
                    dots:true,
                    arrows:false
                  }
                }
              ]
          };
        return(
            <Fragment>
                <section className="reviews-section">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="our-review-heading">
                                    <p>Our Reviews</p>
                                </div>
                            </div>    
                            <div className="col-md-12">
                                <div className="main-tabs">
                                    <div className="tabs-header">
                                        <ul className="no-default">
                                            <li className={ this.state.isActive == "doctor" ? "active" : "" } data-id="doctor" onClick = { this.changeTabs } > Doctors </li>
                                            <li className={ this.state.isActive == "patient" ? "active" : "" } data-id="patient" onClick = { this.changeTabs } > Patients </li>
                                        </ul>
                                    </div>
                                    <div className="tabs-body">
                                        <div className="tabs-inner">
                                         
                                            {
                                                this.state.isActive == "doctor" ? 
                                                   //Doctor 
                                                <div className="doctor-list">
                                                    <Slider {...settings}>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                                <div className="reviews-text">
                                                                    <p>
                                                                    I have been using DocLink for the past month and have seen a great increase in my patient interactions. They are able to contact me outside the clinic which helps me guide them better.
                                                                    </p>
                                                                </div>
                                                                <div className="reviews-details">
                                                                    <div className="review-img">
                                                                        <img src={ DrTalaihaChughtai } />
                                                                    </div>
                                                                    <div className="reviews-names">
                                                                        <h5>DR.TALAIHA CHUGHTAI</h5>
                                                                        <p>PEDIATRICIAN</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                                <div className="reviews-text">
                                                                    <p>
                                                                     DocLink has been beneficial for an additional income. My patients are able to connect with me better than before.
                                                                    </p>
                                                                </div>
                                                                <div className="reviews-details">
                                                                    <div className="review-img">
                                                                        <img src={ DrSamiaHussain } />
                                                                    </div>
                                                                    <div className="reviews-names">
                                                                        <h5>DR SAMIA HUSSAIN</h5>
                                                                        <p>GENERAL MEDICINE</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                This is revolutionary technology. A few months back I wouldn’t have thought of Doctor-Patient communication becoming so seamless. But now my patients are just a click away.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ DrKamranIqbal } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>DR. KAMRAN IQBAL</h5>
                                                                    <p>GENERAL MEDICINE</p>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                                <div className="reviews-text">
                                                                    <p>
                                                                    I have been using DocLink for the past month and have seen a great increase in my patient interactions. They are able to contact me outside the clinic which helps me guide them better.
                                                                    </p>
                                                                </div>
                                                                <div className="reviews-details">
                                                                    <div className="review-img">
                                                                        <img src={ DrTalaihaChughtai } />
                                                                    </div>
                                                                    <div className="reviews-names">
                                                                        <h5>DR.TALAIHA CHUGHTAI</h5>
                                                                        <p>PEDIATRICIAN</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    
                                                    {/* <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d02 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> */}
                                                    </Slider> 
                                                 
                                                </div> 
                                                
                                                //END DOCTOR TAB's
                                            
                                                : 
                                                //Patient TAB's
                                                <div className="patient-list">
                                                    <Slider {...settings}> 
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                My son was sick since 1 week but I have not been able to visit the Doctor because of the lockdown. I talked with my doctor on DocLink and now my son is much better. 
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ patient1 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Khurram Latif</h5>
                                                                    {/* <p>Ophthalmologist AKUH</p> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                I was running a fever but did not know if I should visit the doctor. I connected with my doctor on DocLink instead and was very satisfied with the online consultation they provided.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={patient2 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Zamran Khan</h5>
                                                                    {/* <p>Ophthalmologist AKUH</p> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                Thank you DocLink for making our lives so much easier. Very helpful… must recommend.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ patient3 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Seeta</h5>
                                                                    {/* <p>Ophthalmologist AKUH</p> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                                My son was sick since 1 week but I have not been able to visit the Doctor because of the lockdown. I talked with my doctor on DocLink and now my son is much better. 
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ patient1 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Khurram Latif</h5>
                                                                    {/* <p>Ophthalmologist AKUH</p> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/* <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                               “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                               “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                               “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                               “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="item">
                                                        <div className="reviews-box">   
                                                            <div className="reviews-text">
                                                                <p>
                                                               “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.
                                                                    Velit officia consequat duis enim velit mollit.
                                                                </p>
                                                            </div>
                                                            <div className="reviews-details">
                                                                <div className="review-img">
                                                                    <img src={ d01 } />
                                                                </div>
                                                                <div className="reviews-names">
                                                                    <h5>Dr. Amna Chaudhry</h5>
                                                                    <p>Ophthalmologist AKUH</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> */}
                                                </Slider>  
                                                </div> 
                                                //End Patient TAB's 
                                            }
                                            
                                            

                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div className="review-plus">
                        <img src={ whitePlus } />
                    </div>
                    <div className="review-plus-sm">
                        <img src={ whitePlus } />
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Reviews;