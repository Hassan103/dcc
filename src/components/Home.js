import React, { Component } from 'react';
import Footer from './partials/Footer';
import Header from './partials/Header';
import DownloadApp from './partials/DownloadApp';
import Connect from './partials/Connect';
import Faq from './partials/Faq'; 
import Reviews from './partials/Reviews'
import headerLogo from './../assets/images/headerLogo.png';
import whiteLogo from './../assets/images/whiteLogo.png'


import AboutUsModal from './modals/AboutUsModal';
import PrivacyModal from './modals/PrivacyModal';
import RefundModal from './modals/RefundModal';
import TermsModal from './modals/TermsModal';



class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen : false
        }
    }

    // showLogo = () => {
    //     let get_url =  window.location.href.substr(window.location.href.lastIndexOf('/') +1);
    //     console.log(get_url, "get_url");
    //     if(get_url == "#secondSection" ||  get_url == "#fourthSection"  ){
    //         return <img src={ headerLogo } /> 
    //     }else{
    //         return <img src={ whiteLogo } /> 
    //     }
    // }

    openToggle = () => {
        this.setState({
            isOpen : !this.state.isOpen
        });
        console.log(this.state.isOpen)
    }


    // close Nav
    closeNav = () => {
        this.setState({
            isOpen : false
        })
    }
    render(){
        // this.showLogo();
        return(
            <div>
                 {/* ===================
                        *** Navigation ***
                    ==========================
                     */} 
                    <div className="main-navigaiton">
                        <nav className={ this.state.isOpen ? 'navbar navbar-expand-lg open' : 'navbar navbar-expand-lg' }> 
                        <div className="d-flex align-items-center justify-content-between">
                            <a className="navbar-brand" href="/">  
                            <div className="color-logo">
                                {
                                    this.state.isOpen ? <img src={ whiteLogo } />    : <img src={ headerLogo } />
                                } 
                            </div>
                            <div className="white-logo">
                                <img src={ whiteLogo } /> 
                            </div>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={ this.openToggle }>
                            <span className="">
                                <div id="nav-icon1" className={ this.state.isOpen ? 'open' : ''  }>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </span>
                        </button>
                        </div>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav desktop-nav">
                                <li className="nav-item" data-menuanchor="firstSection" onClick={ this.closeNav }>
                                    <a className="nav-link" href="#firstSection">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item" data-menuanchor="secondSection" onClick={ this.closeNav }>
                                    <a className="nav-link" href="#secondSection"> Our Reviews </a>
                                </li>
                                <li className="nav-item" data-menuanchor="thirdSection" onClick={ this.closeNav }>
                                    <a className="nav-link" href="#thirdSection"> Download App </a>
                                </li>
                                <li className="nav-item" data-menuanchor="fourthSection" onClick={ this.closeNav }>
                                    <a className="nav-link" href="#fourthSection">FAQ's</a>
                                </li>
                                <li className="nav-item" data-menuanchor="fifthSection" onClick={ this.closeNav }>
                                    <a className="nav-link" href="#fifthSection">Connect</a>
                                </li> 
                                </ul> 
                            </div>
                        </nav>
                    </div>
                    {/* ===================
                        *** End Navigation ***
                    ==========================
                     */}
                <div id="fullpage">
                    <section className="section fp-auto-height-responsive" id="section1">
                        <Header />
                    </section>
                    <section className="section fp-auto-height-responsive" id="section2">
                        <Reviews />  
                    </section>
                    <section className="section fp-auto-height-responsive" id="section3">
                        <DownloadApp />
                    </section>
                    <section className="section fp-auto-height-responsive" id="section4">
                        <Faq />
                    </section>
                    <section className="section fp-auto-height-responsive" id="section5">
                        <Connect /> 
                    </section>
                    <section className="section fp-auto-height" id="section6">
                        <Footer />
                    </section>
                </div>

                {/* 
                <Header />
                <Reviews />  
                <DownloadApp />
                <Faq />
                <Connect /> 
                <Footer /> */}
                
 
                <div class="contact-form">
                <div class="modal fade" id="contactForm" tabindex="-1" aria-labelledby="contactFormLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="close_btn">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="contact-form-wrapper">
                                    <div class="modal-heading">
                                        <h1> <span>Send</span> us a Message</h1>
                                    </div>  
                                    <div class="contact-main-form">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <input input="text" class="form-control" placeholder="First Name" />
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <input input="text" placeholder="Last Name" class="form-control" />
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group mb-2">
                                                            <select id="phones">
                                                                <option value="+92">+92</option> 
                                                            </select>
                                                            <input type="text" class="form-control phone-num" placeholder="Phone Number" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input input="email" placeholder="Email Address" class="form-control" />
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <textarea placeholder="Your Message" class="form-control"></textarea>
                                                        </div>
                                                        <div class="contact-form-btn">
                                                            <input type="submit" value="SEND" className="contact-btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>    
    


        {/* About Modal */}
            {/* <AboutUsModal /> */}
            <PrivacyModal />
            <RefundModal />
            <TermsModal />
        {/* End About Modal */}

            </div>
        )
    }
}

export default Home;