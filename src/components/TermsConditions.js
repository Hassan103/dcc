import React, { Component, Fragment } from 'react';
import headerLogo from './../assets/images/headerLogo.png'; 
import Footer from './partials/Footer';
class TermsConditions extends Component{
    render(){
        return(
            <Fragment>
                <div className="header-Inner">
                    <div className="main-navigaiton">
                        <nav className="navbar navbar-expand-lg">
                        <a className="navbar-brand" href="#"> <img src={ headerLogo } /> </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav desktop-nav">
                                <li className="nav-item" data-menuanchor="firstSection">
                                    <a className="nav-link" href="/#firstSection">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item" data-menuanchor="secondSection">
                                    <a className="nav-link" href="/#secondSection"> Our Reviews </a>
                                </li>
                                <li className="nav-item" data-menuanchor="thirdSection">
                                    <a className="nav-link" href="/#thirdSection"> Download App </a>
                                </li>
                                <li className="nav-item" data-menuanchor="fourthSection">
                                    <a className="nav-link" href="/#fourthSection">FAQ's</a>
                                </li>
                                <li className="nav-item" data-menuanchor="fifthSection">
                                    <a className="nav-link" href="/#fifthSection">Connect</a>
                                </li> 
                                </ul> 
                            </div>
                        </nav>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="inner-page-heading-text">
                                <h1>
                                    Terms And Conditions
                                </h1>
                            </div>
                        </div>
                    </div> 
                </div>
                    {/* Inner Page TextAtrea */}
                    <div className="">
                        <div className="inner-page-text">
                            <div className="container">
                                <div className="row">
                                    <div className="page-text">
                                            <h1>TERMS OF USE</h1>
                                            <h4>Effective date:</h4>
                                            <h5>1st April - 2020</h5>
                                            <br/>
                                            <h6>DocLink operates an online healthcare platform for healthcare service providers, hereby referred to as doctors, to manage their services and engage with their patients, hereafter referred to as the ‘DocLink’ Mobile Application. “We”, “our” and “us” currently refers to DocLink in the terms defined below. These terms are between DocLink and its Customers, hereafter referred to as “you” or collectively as “Customers”.</h6>
                                            <br/>
                                            <h6>
                                            The DocLink Mobile Application supports the goal of continuous care for health by providing healthcare service providers who are our Customers, with an online Clinic (involving associated mobile applications and hereafter collectively referred to as the ‘Virtual Practice’) that enables them to be securely accessible to their patients, through web and mobile services enabling online consultations and remote patient monitoring. The Virtual Practice applications are intended only as tools, which help healthcare providers and their patients to collaborate in the healthcare of the patients, using communication, scheduling, administrative and data managing capabilities. Virtual Practice also enables online presence management for better patient engagement.
                                            </h6>
                                            <br/>
                                            <h6>
                                            If you do not agree to these Terms of Use and/or are not eligible or authorized to agree to these Terms of Use, then you may not register for and set up a Virtual Practice on the DocLink platform and are not authorized to use any of the applications or services supported by DocLink’s Platform.
                                            </h6>
                                            <br/>
                                        <h1>Doctors</h1>
                                        <br/>
                                        <h6>
                                        Healthcare service providers can set up a Virtual Practice on the DocLink platform (hereafter referred to as ‘Doctors). If a Virtual Practice is being set up by an individual healthcare service provider who is not formally affiliated with an organization, the Doctor is the individual creating the Virtual Practice. If the Virtual Practice is being set up for a healthcare services organization by an authorized representative, the organization is the Customer.
                                        </h6>
                                        <br/>
                                        <h1>Authorized Users</h1>
                                        <h6>Registering for and setting up a Virtual Practice (and all its integral apps) requires full acceptance of these Terms of Use and all associated policies. By registering, you agree that:</h6>
                                        <br/>
                                        <p>You are a licensed healthcare provider and/or authorized representative of a licensed healthcare organization with the necessary healthcare practice license and legal approvals to provide healthcare services to patients that you will provide services to through your Virtual Practice.</p>
                                        
                                        <p>
                                        You shall not register and use DocLink’s product and any other of the DocLink Mobile Application for any purpose other than providing services to authorized Patients Users seeking your services.
                                        </p>
                                        <p>
                                        You are not impersonating any other person and are using your actual identity.
                                        </p>
                                        <p>
                                        The information provided during the registration process is true, accurate, current and complete.
                                        </p>
                                        <p>
                                        You are required to periodically review and update your registration data to ensure that it is always current and correct.
                                        </p>
                                        <p>
                                        Usage of the DocLink Mobile Application by Doctors and Patient Users are subject to acceptance of these Terms of Use. These Terms of Use a redeemed to include the Privacy Policy, Authorized User Terms, Acceptable Usage Policy, and all conditions, policies and operating procedures that are referred to herein or which may otherwise be published by DocLink from time to time, on the DocLink platform applications
                                       </p>
                                       <h1>Doctors Data</h1>
                                       <h6>By using your Virtual Practice and associated applications, your Patient Users understand that the healthcare providers in the Virtual Practice have access to view and update their healthcare data in the course of providing them with healthcare services. Ownership of and responsibility for healthcare data of patients in a Virtual Practice rests with the Doctor.</h6>
                                       <br/>
                                       <h6>As owner and controller of the Patient Data, the Doctor alone may provide us with instructions on what to do with this data. All Patient Data is covered by the DocLink platform’s Privacy Policy.</h6>
                                       <br/>
                                       <h1>Acceptable Usage Policy</h1>
                                       <h6>This Acceptable Use Policy sets out a list of acceptable and unacceptable conduct for our DocLink Mobile Application by Doctors and Authorized Users in the Virtual Practices of our Doctors.</h6>
                                       <br/>
                                       <h6>Any violation of the policy could result in the suspension of your access to the Platform Services and if found to be deliberate, repeated and possibly harmful to other users of the platform will result in termination of your access to the DocLink Platform Services. This policy may change as the platform evolves, so please proactively look up updates to the policy.</h6>
                                       <br/>
                                       <h6>The pin code that you receive on each login attempt is unique to you, and you agree not to disclose or share it with any third party. You are responsible for keeping your pin code confidential and for notifying us immediately if your pin code has been hacked or stolen.</h6>
                                       <br/>
                                       <h6>You also agree that you will be solely responsible for any activities conducted on or through the DocLink Mobile Application with your account regardless of whether or not you are the individual who undertakes such activities. This includes any unauthorized access and/or use of your account or your mobile device. You hereby release and hold DocLink harmless from any claims and causes of action arising out of or resulting from a third party’s unauthorized use of your account.</h6>
                                       <br/>
                                       <h6>Your use and access of the DocLink platform, its apps, and services, is entirely at your initiative and risk. You are solely responsible for compliance with local laws in your respective country concerning the use of such services. DocLink does not claim that the DocLink Mobile Application is fully compliant for use within all the countries and territories from where it may be accessed.</h6>
                                       <br/>
                                       <h6>From time to time, some of our users do provide us with feedback on recommended changes and/or corrections to our platform and apps. Such recommendations are sometimes accepted if they fit in with our overall product roadmap. If you provide such feedback, you warrant that it does not include any person or entity’s proprietary information. You also fully agree that DocLink is not obliged to use or act on this feedback and even if it does, it has full, royalty-free, perpetual, irrevocable, transferable and global license to use this feedback.</h6>
                                       <br/>
                                       <h6>DocLink is constantly updating the DocLink platform and apps to provide the best possible experience for its users. All users acknowledge and agree that the form and nature of the services provided by the platform may change from time to time, features may be added, improved or changed without prior notice as per the demands of changing technology, domain and the best interests of the users.</h6>
                                       <br/>
                                       <h6>You shall not use the DocLink platform and its apps and services for any purposes other than those intended by DocLink and as determined by DocLink at its sole discretion.</h6>
                                       <br/>
                                       <h6>You shall not attempt to circumvent any security measure put in place for the DocLink platform users or attempt to gain unauthorized access to any services, user accounts, or computer systems or networks owned and/or controlled by DocLink, through hacking, password mining or any other means.</h6>
                                       <br/>
                                       <h6>You shall not upload or submit any data or information containing viruses or any computer code, corrupt files or programs engineered or intended to disrupt or destroy the functionality of any software, hardware, telecommunications, networks, servers or other equipment.</h6>
                                       <br/>
                                       <h6>You shall not attempt to access, access or obtain information regarding any account other than your own, by any means whatsoever. You may not use the DocLink platform and its apps and services in any manner that could damage, disable, overload, or impair our servers or networks, or interfere with any other party’s use and enjoyment of the site and services.</h6>
                                       <br/>
                                       <h6>You may not use any robot, spider, scraper or other automated means to access the DocLink Mobile Application or website for any purpose without our written permission. Violation of this policy may result in termination of your access to the site, deactivation or deletion of your registration and all related information and files you have stored on the platform.</h6>
                                       <br/>
                                       <h6>If you are a competitor service, you shall not use this service as a user to study and copy features and services provided by DocLink’s platform, apps, and services.</h6>
                                       <br/>
                                       <h6>To protect the integrity of its DocLink website and Platform Services, DocLink reserves the right at any time in its sole discretion to block users from certain IP addresses.</h6>
                                       <br/>
                                       <h6>You are responsible for the information you voluntarily post and agree to defend (at your sole expense), indemnify and hold DocLink harmless from any damages, losses, costs, or expenses which DocLink may incur as a result of information you post.</h6>  
                                       <br/>
                                       <h6>We may offer certain features to you at no charge or a special reduced charge, for beta trial use. Your use of beta features is subject to any additional terms that we specify when enabling them for you. The use of such features is permitted only during the specified duration and/or when the feature is terminated. Such features are not guaranteed to be released and may on occasion be terminated. Such features are generally still under development and/or not completely integrated into the platform and therefore may contain more issues or errors than is generally noted in the product. Should you choose not to subscribe to the plans under which the Beta Features will be eventually supported, or if we choose to terminate such Beta Features, you will lose access to content created as a result of using such Beta Features. We disclaim all obligations and liabilities concerning such features. The information about such Beta features and their capabilities are limited to customers to whom they are made especially available and this is covered by the confidentiality clause in our Terms of Use.</h6>
                                       <br/>
                                       <h1>Doctor Responsibilities</h1> 
                                       <br/>
                                       <h6>In addition to the above general terms of acceptable use, the following responsibilities apply to all Doctors.</h6> 
                                       <br/>
                                       <p>The Doctor is responsible for all the settings and configuration applied in the Virtual Practice, including management of services and provision of those services and management of Patient Data.</p> 
                                       <p>You are completely responsible for the Customer Data in your Virtual Practice including ensuring the usage practices by you and your Team Users in maintaining the privacy and confidentiality of your patients’ healthcare data in your Virtual Practice.</p>
                                       <p>You will comply with healthcare and data regulation rules applicable to healthcare providers and organizations in your country of business and locations of service. You are solely responsible for choosing to use the Platform Services after ascertaining that it meets the necessary regulatory compliance applicable to your operations.</p>   
                                       <p>You will not publish or distribute in any form, any patient healthcare information without their informed consent. DocLink is not liable for the handling of your data and nor for any outcomes as a result of that.</p>  
                                       <p>The Doctor will be responsible for all the activities in his/her VirtualPractice account.</p>  
                                       <p>The DocLink Platform serves only as a technology enabler for the online healthcare services that you choose to provide. Any disputes between you and the Patient Users of your Virtual Practice will be dealt with by you directly. DocLink has no obligation or liability to monitor services within Virtual Practices.</p> 
                                       <p>DocLink takes no responsibility for healthcare and/or other information that may be published by healthcare providers in their Virtual Practice. Healthcare providers are completely liable to ensure that such content is medically correct, original and that it causes no intended or unintended harm to its readers.</p> 
                                       <p>You unconditionally agree to receive certain essential automated SMS, mobile and email notifications relevant to system events from the DocLink Mobile Application apps to the mobile number and email address associated with your user account, irrespective of whether you have subscribed to any Do-Not-Disturb (DND) services.</p> 
                                       <p>All Patient Users of a Virtual Practice of a Doctor are to be made aware of the following by the Customer and Team Users and through Patient Terms defined in the Virtual Practice:</p> 
                                       <p>Patient Users of a Virtual Practice shall not use any service or information in their healthcare provider’s Virtual Practice for any purposes other than managing their personal health information or that of their immediate family members (with due authorization) and/or to engage with healthcare providers in the Virtual Practice.</p> 
                                       <p>DocLink does not provide medical services of any kind and as a technology provider only enables engagement with you their healthcare provider, through the Virtual Practice, which you the Customer have opted to use.</p> 
                                       <p>DocLink does not recommend or endorse any healthcare providers or services using the Virtual Practice.</p>
                                       <p>Patient Users’ access or use of their healthcare provider’s Virtual Practice and/or any apps and services of the DocLink platform does not create in any way, a relationship that may be confidential or privileged or of any kind that would obligate DocLink to fulfil any duties towards meeting their health needs.</p>  
                                       <p>Patient users are expected to avail of immediate professional medical attention in medical emergencies or critical care situations and not attempt to use the Virtual Practice and/or other apps and services of the DocLink platform in such situations.</p> 
                                       <p>DocLink is not liable for any use or misuse of their health data by your healthcare provider.</p>  
                                       <p>DocLink shall not be responsible for any undesirable outcomes in their engagement with you their healthcare provider.</p>
                                       <br/>   
                                       <h1>Subscriptions & Payments</h1>   
                                       <h6>The platform usage varies for different users. Patients use the platform by purchasing different subscription packages created by doctors whereas doctors are charged a fixed percentage from all the earnings which they receive from their sold packages.</h6> 
                                       <br/>
                                       <h6>
                                       The following platform services are currently provided free of charge but DocLink reserves the right to charging for them at cost based on actual usage, at any point in its choosing.</h6>  
                                       <br/>
                                       <p>Verification SMS and Mobile Notifications</p>  
                                       <p>Data Storage Charges</p>  
                                       <br/>
                                       <h6>Subscriptions are managed and paid for either through cash or online transactions by Customers, who can choose to subscribe on a daily, weekly, monthly, annual or pay-per interaction plan.</h6>
                                       <br/>
                                       <h6>All subscriptions must be manually renewed at the end of the billing cycle for the upcoming subscription periods equal to the preceding subscription period, and the per Team User pricing during any manual renewal term will be based on the amount of subscription set by Doctor at the point of renewal. All subscription payments are paid forward for the upcoming subscription term.</h6> 
                                       <br/>
                                       <h1>Updates</h1>
                                       <br/>
                                       <h6>DocLink may change these Terms of Use (and any associated Policies) at any time, at its sole discretion, in whole or part. In the event of any material or substantial change in the terms and conditions DocLink may notify you by posting an announcement of the changes on its website, through its applications or by email. You are expected to periodically check these Terms of Use and associated policies for changes. By continuing to use the DocLink Mobile Application after any change to these Terms of Use, you acknowledge and accept the changes. The Terms of Use as they apply to you as a user, may not otherwise be amended.</h6>  
                                       <br/> 
                                       <h1>Intellectual Property, Copyright, and Trademarks</h1>
                                       <br/>
                                       <h6>DocLink’s platform services, all content, and information, visual designs, and branding created by and for DocLink, and all intellectual property rights embodied therein, are the property of DocLink and are protected by trademarks, copyrights, patents, proprietary rights, and IP laws.</h6>
                                       <br/>
                                       <h6>DocLink does not grant any rights to copy, use, modify, reproduce, adapt, distribute or create derived works of any part of its DocLink platform, apps, content, website, and design. Any reproduction of the contents, in whole or in part, regardless of the procedure or the medium used, shall require the express prior written authorization of DocLink.</h6>
                                       <br/>
                                       <h6>Attempting to use or access the DocLink platform, apps, services, and website for any purposes other than intended as per these Terms of Use is prohibited. DocLink reserves the right at any time in its sole discretion to block and terminate users violating these terms, in addition to taking legal action where necessary.</h6>
                                       <br/>
                                       <h1>Limited Warranty</h1>
                                       <br/>
                                       <h6>To the extent allowed by applicable law, implied warranties on the Software, DocLink software, and services are provided without any warranty of any kind, and DocLink hereby disclaims all express or implied warranties, including without limitation warranties of merchantability, fitness for a particular purpose, quality, performance, accuracy, reliability, loss or corruption of data, business interruption or any other commercial damages or losses, arising out of or related to the software. DocLink makes no warranty that the services will be available and accessible uninterrupted or error-free or otherwise meet your expectations. This disclaimer of warranty constitutes an essential part of this agreement.</h6>
                                       <br/>
                                       <h6>To the maximum extent permitted by applicable law, the above warranty is exclusive and instead of all other warranties, whether express or implied, including the implied warranties of merchantability, fitness for a particular purpose, and noninfringement of intellectual property rights.</h6>
                                       <br/>
                                       <h1>Limitation of Liability</h1>
                                       <br/>
                                       <h6>The users expressly understand and agree that DocLink and its subsidiaries, affiliates, officers, employees, agents, partners, and licensors shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including, but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if DocLink has been advised of the possibility of such damages), resulting from (i) the use or the inability to use the service; (ii) unauthorized access to or alteration of your transmissions or data; (iii) statements or conduct of any third party on the service; or (iv) any changes which DocLink may make to the services (v) your failure to keep your pin code and account details secure and confidential;(vi) any other matter relating to the service.</h6>
                                       <br/>
                                       <h6>The limitations on DocLink’s liability to the User shall apply whether or not DocLink has been advised of or should have been aware of the possibility of any such losses arising.</h6>
                                       <br/>
                                       <h1>Indemnification and Release</h1>
                                       <br/>
                                       <h6>You agree to defend, indemnify and hold harmless DocLink, and its officers, managers, members, directors, employees, successors, assigns, subsidiaries, affiliates, service professionals, suppliers, and agents, from and against any claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorneys’ fees) arising from your use of, access to, and participation in services provided by the website; your violation of any provision of the terms of use, including the privacy policy; your violation of any third-party right, including without limitation any copyright, property, proprietary, intellectual property; or any claim that your submitted content has caused damage to a third party. This defence and indemnification obligation will survive the terms of service and the termination of your account.</h6>
                                       <br/>
                                       <h1>Termination</h1>
                                       <h6>These Terms of Use take effect from the time you start using DocLink’s Mobile Application in any capacity until it is terminated by you and/or DocLink as provided below.</h6>
                                       <br/>
                                       <h6>All your paid subscriptions to the doctors and/or any of the DocLink Mobile Application expires due to a cancelled subscription and/or expiry of a subscription that was not in auto-renewal mode.</h6>
                                       <br/>
                                       <h6>DocLink terminates your account and access to the DocLink Mobile Application due to breach of these Terms of Use by you and/or any Authorized Users of your account and associated Platform Services. Such termination may be with immediate effect.</h6>
                                       <br/>
                                       <h6>DocLink reserves the right to modify and/or discontinue its services at any time, for any reason including but not limited to violation of this agreement. In cases where the cause is not due to violation of this agreement (Terms of Use), at least thirty (30) days prior notice will be provided.</h6>
                                       <br/>
                                       <h6>On termination of your account, you will no longer be able to use your account or any Platform Services of the platform. Any obligations you may have before the effective date of termination must be met. Any termination does not relieve any user of the obligation to pay any fees payable to us for the period before the effective date of termination.</h6>
                                       <br/>
                                       <h6>In the case of terminations with cause, there will be no refund of subscription payments, for the period between the date of termination and date of expiry of the current subscription. DocLink also retains the right to pursue any action to remedy the breach of these Terms of Use.</h6>
                                       <br/>
                                       <h1>Assignment</h1>
                                       <br/>
                                       <h6>Neither party may assign or delegate any of its rights or obligations hereunder, whether by operation of law or otherwise, without the prior written consent of the other party (not to be unreasonably withheld). Notwithstanding the foregoing, either party may assign the Agreement in its entirety (including all order forms), without consent of the other party, to a corporate affiliate or in connection with a merger, acquisition, corporate reorganization, or sale of all or substantially all of its assets. The customer will keep its billing and contact information current at all times by notifying DocLink of any changes. Any purported assignment in violation of this section is void. A party’s sole remedy for any purported assignment by the other party in breach of this section will be, at the non-assigning party’s election, termination of the Contract upon written notice to the assigning party. Subject to the foregoing, this Agreement will be binding upon, and inure to the benefit of, the successors, representatives, and permitted assigns of the parties.</h6>
                                       <br/>
                                       <h1>Jurisdiction and Governing Law</h1>
                                       <br/>
                                       <h6>These terms shall be governed by and construed following the laws of Pakistan without reference to conflict of laws principles, and disputes arising in relation hereto shall be subject to the exclusive jurisdiction of the courts at Karachi, Sindh, Pakistan. These Terms of Use and associated policies shall be admissible in all legal proceedings. Use of the DocLink platform and its services is not authorized in any territory of jurisdiction that does not give effect to all provisions of these Terms of Use, including this section.</h6>
                                       <br/>
                                       <h1>Release for Force Majeure</h1>
                                       <br/>
                                       <h6>DocLink and its officers, directors, employees, agents, content providers, customers and suppliers shall be absolved from any claim or damages resulting from any cause(s) over which DocLink or they do not have direct control, including, but not limited to, failure of electronic or mechanical equipment or communication lines, telephone or other interconnect problems, computer viruses or other damaging code or data, unauthorized access, theft, operator errors, severe weather, earthquakes, natural disasters, strikes or other labour problems, wars, or governmental restrictions.</h6>
                                       <br/>
                                       <h1>General Provisions</h1>
                                       <br/>
                                       <h6>This Agreement contains the entire agreement and understanding of the parties concerning the subject matter hereof and supersedes all prior written and oral understandings of the parties concerning the subject matter thereof. You may not assign or sub-license any of the rights and obligations under this Agreement without the prior written consent of DocLink. Any notices in this regard need to be delivered in written format and acknowledged as received. DocLink may subcontract its responsibilities under this Agreement, without your consent to a third party considered by DocLink in good faith to be of equal standing and integrity provided that material provisions of this Agreement shall be reflected in any agreement entered into between DocLink and such third party. No partnership, joint venture, agency or employment relationship is created as a result of these Terms of Use, and neither party has any authority of any kind to bind the other in any respect. If for any reason any provision of this Agreement is held invalid or otherwise unenforceable, such invalidity or unenforceability shall not affect the remainder of this Agreement, and this Agreement shall continue in full force and effect to the fullest extent allowed by law. The parties knowingly and expressly consent to the foregoing terms and conditions.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <Footer />
            </Fragment>
        );
    }
}


export default TermsConditions;

