import React, { Component, Fragment } from 'react';
import headerLogo from './../assets/images/headerLogo.png'; 
import Footer from './partials/Footer';
import {Link} from 'react-router-dom';

class AboutUs extends Component{


    render(){
        return(
            <Fragment>
                <div className="header-Inner">
                    <div className="main-navigaiton">
                        <nav className="navbar navbar-expand-lg">
                        <a className="navbar-brand" href="/"> <img src={ headerLogo } /> </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav desktop-nav">
                                <li className="nav-item" data-menuanchor="firstSection">
                                    <a className="nav-link" href="/#firstSection">Home <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item" data-menuanchor="secondSection">
                                    <a className="nav-link" href="/#secondSection"> Our Reviews </a>
                                </li>
                                <li className="nav-item" data-menuanchor="thirdSection">
                                    <a className="nav-link" href="/#thirdSection"> Download App </a>
                                </li>
                                <li className="nav-item" data-menuanchor="fourthSection">
                                    <a className="nav-link" href="/#fourthSection">FAQ's</a>
                                </li>
                                <li className="nav-item" data-menuanchor="fifthSection">
                                    <a className="nav-link" href="/#fifthSection">Connect</a>
                                </li> 
                                </ul> 
                            </div>
                        </nav>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="inner-page-heading-text">
                                <h1>
                                    About Us
                                </h1>
                            </div>
                        </div>
                    </div> 
                </div>
                    {/* Inner Page TextAtrea */}
                    <div className="">
                        <div className="inner-page-text">
                            <div className="container">
                                <div className="row">
                                    <div className="page-text">
                                        <h1>About Us</h1>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p> In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.</p>
                                        
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam.
                                        </p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <Footer />
            </Fragment>
        );
    }
}


export default AboutUs;

