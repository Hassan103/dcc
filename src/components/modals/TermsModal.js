import React, { Component } from 'react';


class TermsModal extends Component{
  constructor(props){
    super(props);

  }
  render(){
    return(        
        <div className="customModal">
          <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content ">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TERMS & CONDITIONS</h5>
                <div className="close_btn">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
              </div>
              <div class="modal-body">
                <div class="page-text">
   <h1>REFUND &amp; RECONCILIATION POLICY</h1>
   <h6>All users of DocLink Mobile Application are bound by this Refund &amp; Reconciliation Policy.</h6>
   <h4>Effective date:</h4>
   <h5>1st July 2020</h5>
  
   <h1>A. For Patients:</h1>
   <h5>a. Wallet Amount Not Updated Accurately</h5>
  
   <p>i. In case the patient's wallet amount is not updated accurately then the patient can use the “Report A Problem” feature available in the app and create a refund request by sending their details under the “payment” category. The refund request will be considered only if the request is made within 24 hours of the said wallet recharge.</p>
   <p> ii. Once your request is received, we will notify you through email that we have received your request and are working on it. Furthermore, we will also notify you upon the approval or rejection of your refund. If your refund is approved, then the relevant amount will be automatically adjusted in your in-app wallet, within 3 working days.</p>
  
   <h5>b. Amount Deduction Of Purchased Package</h5>
  
   <p>i. In case any amount is overcharged from patient’s wallet while they purchase any doctor package then the patient can use the “Report A Problem” feature available in the app and create a refund request by sending their details under the “payment” category. The refund request will be considered only if the request is made within 24 hours of package procurement.</p>
   <p>ii. Once your request is received, we will notify you through email that we have received your request and are working on it. Furthermore, we will also notify you upon the approval or rejection of your refund. If your refund is approved, then the relevant amount will be automatically adjusted in your in-app wallet, within 3 working days.</p>
  
   <h5>c. Incomplete Chat Sessions</h5>
  
   <p>i. If due to a network issue from the Doclink Server side, you are not able to complete the chat session then the patient can use the “Report A Problem” feature available in the app and create a refund request by sending their details under the “payment” category. The refund request will be considered only if the request is made within 24 hours of the said incomplete session.</p>
   <p>ii. Once your request is received, we will notify you through email that we have received your request and are working on it. Furthermore, we will also notify you upon the approval or rejection of your refund. If your refund is approved, then the amount deducted for that session might be fully or partially added back in your in-app wallet within 3 working days</p>
  
   <h5>d. Completed Chat Sessions</h5>
  
   <p>i. We will be evaluating these on a case-to-case basis. To request a refund, the patient can send the details of the incident under “payment” category in “Report a Problem”. We will use Chief Complaint and Doctor’s Note to evaluate how well the query was answered.</p>
   <p>ii. Once your request is received, we will notify you through email that we have received your request and are working on it. Furthermore, we will also notify you upon the approval or rejection of your refund. If your refund is approved, then the amount deducted for that session might be fully or partially added back in your in-app wallet.</p>
  
   <h5>e. Late or Missing Refunds</h5>
  
   <p>i. If you haven't received a refund yet, first check your app wallet history again. In some cases, it may take some additional time before your refund is added If you've waited for additional days and you still have not received your refund yet, please contact us at hello@doclink.health</p>
  
   <h1>A. For Patients:</h1>
  
   <h5>a. Doctor Earnings Not Updated Accurately</h5>
  
   <p>i. In case the doctor's earnings are not updated accurately then the doctor can use the “Report A Problem” feature available in the app and create a refund request by sending their details under the “payment” category. The refund request will be considered only if the request is made within 24 hours of the said instance.</p>
   <p>ii. Once your request is received, we will notify you through email that we have received your request and are working on it. Furthermore, we will also notify you upon the approval or rejection of your complain. If your complain is approved, then the relevant amount will be reconciliated within 5 working days.</p>
</div>


              </div> 
            </div>
          </div>
          </div>
        </div>
        
    )
  } 
}


export default TermsModal;