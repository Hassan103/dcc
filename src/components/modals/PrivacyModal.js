import React, { Component } from 'react';


class PrivacyModal extends Component{
  constructor(props){
    super(props);

  }
  render(){
    return(        
        <div className="customModal">
          <div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content ">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">PRIVACY POLICY</h5>
                <div className="close_btn">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
              </div>
              <div class="modal-body">
                   <div class="page-text">
 
   <h6>All users of Doclink mobile application are bound by this privacy policy.</h6>
   <h4>Effective date: 1st April - 2020</h4>
   
   
   <h1>Third-Party Services</h1>
   
   <h6>DocLink is not responsible for the accuracy and reliability of health advice offered by different doctors. This includes direct the data transfer through messages (via Chat) or data transferred in the shape of images. Furthermore, DocLink doesn’t endorse or recommends any doctor that is available on the DocLink platform.</h6>
   
   <h6>Nexus is also not responsible for the functioning and reliability of any third-party services (including payment gateways and SMS service) that you may choose to interface with now or in the future (from among authorized third-party interfaces) to further the functionality of your account. Terms of use applicable to those services must be complied with, in addition to DocLink’s Terms of Use.</h6>
   
   <h1>Updates</h1>
   
   <h6>DocLink may change these Terms of Use (and any associated Policies) at any time, at its sole discretion, in whole or part. In the event of any material or substantial change in the terms and conditions DocLink may notify you by posting an announcement of the changes on its website, through its applications or by email. You are expected to periodically check these Terms of Use and associated policies for changes. By continuing to use the DocLink Mobile Application after any change to these Terms of Use, you acknowledge and accept the changes. The Terms of Use as they apply to you as a user, may not otherwise be amended.</h6>
   
   <h1>Intellectual Property, Copyright, and Trademarks</h1>
   
   <h6>DocLink’s platform services, all content, and information, visual designs, and branding created by and for DocLink, and all intellectual property rights embodied therein, are the property of DocLink and are protected by trademarks, copyrights, patents, proprietary rights, and IP laws.</h6>
   
   <h6>DocLink does not grant any rights to copy, use, modify, reproduce, adapt, distribute or create derived works of any part of its DocLink platform, apps, content, website, and design. Any reproduction of the contents, in whole or in part, regardless of the procedure or the medium used, shall require the express prior written authorization of DocLink.</h6>
   
   <h6>Attempting to use or access the DocLink platform, apps, services, and website for any purposes other than intended as per these Terms of Use is prohibited. DocLink reserves the right at any time in its sole discretion to block and terminate users violating these terms, in addition to taking legal action where necessary.</h6>
   
   <h1>Limited Warranty</h1>
   
   <h6>To the extent allowed by applicable law, implied warranties on the Software, DocLink software, and services are provided without any warranty of any kind, and DocLink hereby disclaims all express or implied warranties, including without limitation warranties of merchantability, fitness for a particular purpose, quality, performance, accuracy, reliability, loss or corruption of data, business interruption or any other commercial damages or losses, arising out of or related to the software. DocLink makes no warranty that the services will be available and accessible uninterrupted or error-free or otherwise meet your expectations. This disclaimer of warranty constitutes an essential part of this agreement.</h6>
   
   <h6>To the maximum extent permitted by applicable law, the above warranty is exclusive and instead of all other warranties, whether express or implied, including the implied warranties of merchantability, fitness for a particular purpose, and noninfringement of intellectual property rights.</h6>
   
   <h1>Limitation of Liability</h1>
   
   <h6>The users expressly understand and agree that DocLink and its subsidiaries, affiliates, officers, employees, agents, partners, and licensors shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including, but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if DocLink has been advised of the possibility of such damages), resulting from (i) the use or the inability to use the service; (ii) unauthorized access to or alteration of your transmissions or data; (iii) statements or conduct of any third party on the service; or (iv) any changes which DocLink may make to the services (v) your failure to keep your pin code and account details secure and confidential;(vi) any other matter relating to the service.</h6>
   
   <h6>The limitations on DocLink’s liability to the User shall apply whether or not DocLink has been advised of or should have been aware of the possibility of any such losses arising.</h6>
   
   <h1>Indemnification and Release</h1>
   
   <h6>You agree to defend, indemnify and hold harmless DocLink, and its officers, managers, members, directors, employees, successors, assigns, subsidiaries, affiliates, service professionals, suppliers, and agents, from and against any claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorneys’ fees) arising from your use of, access to, and participation in services provided by the website; your violation of any provision of the terms of use, including the privacy policy; your violation of any third-party right, including without limitation any copyright, property, proprietary, intellectual property; or any claim that your submitted content has caused damage to a third party. This defence and indemnification obligation will survive the terms of service and the termination of your account.</h6>
   
   <h1>Termination</h1>
   <h6>These Terms of Use take effect from the time you start using DocLink’s Mobile Application in any capacity until it is terminated by you and/or DocLink as provided below.</h6>
   
   <h6>All your paid subscriptions to the doctors and/or any of the DocLink Mobile Application expires due to a cancelled subscription and/or expiry of a subscription that was not in auto-renewal mode.</h6>
   
   <h6>DocLink terminates your account and access to the DocLink Mobile Application due to breach of these Terms of Use by you and/or any Authorized Users of your account and associated Platform Services. Such termination may be with immediate effect.</h6>
   
   <h6>DocLink reserves the right to modify and/or discontinue its services at any time, for any reason including but not limited to violation of this agreement. In cases where the cause is not due to violation of this agreement (Terms of Use), at least thirty (30) days prior notice will be provided.</h6>
   
   <h6>On termination of your account, you will no longer be able to use your account or any Platform Services of the platform. Any obligations you may have before the effective date of termination must be met. Any termination does not relieve any user of the obligation to pay any fees payable to us for the period before the effective date of termination.</h6>
   
   <h6>In the case of terminations with cause, there will be no refund of subscription payments, for the period between the date of termination and date of expiry of the current subscription. DocLink also retains the right to pursue any action to remedy the breach of these Terms of Use.</h6>
   
   <h1>Assignment</h1>
   
   <h6>Neither party may assign or delegate any of its rights or obligations hereunder, whether by operation of law or otherwise, without the prior written consent of the other party (not to be unreasonably withheld). Notwithstanding the foregoing, either party may assign the Agreement in its entirety (including all order forms), without consent of the other party, to a corporate affiliate or in connection with a merger, acquisition, corporate reorganization, or sale of all or substantially all of its assets. The customer will keep its billing and contact information current at all times by notifying DocLink of any changes. Any purported assignment in violation of this section is void. A party’s sole remedy for any purported assignment by the other party in breach of this section will be, at the non-assigning party’s election, termination of the Contract upon written notice to the assigning party. Subject to the foregoing, this Agreement will be binding upon, and inure to the benefit of, the successors, representatives, and permitted assigns of the parties.</h6>
   
   <h1>Jurisdiction and Governing Law</h1>
   
   <h6>These terms shall be governed by and construed following the laws of Pakistan without reference to conflict of laws principles, and disputes arising in relation hereto shall be subject to the exclusive jurisdiction of the courts at Karachi, Sindh, Pakistan. These Terms of Use and associated policies shall be admissible in all legal proceedings. Use of the DocLink platform and its services is not authorized in any territory of jurisdiction that does not give effect to all provisions of these Terms of Use, including this section.</h6>
   
   <h1>Release for Force Majeure</h1>
   
   <h6>DocLink and its officers, directors, employees, agents, content providers, customers and suppliers shall be absolved from any claim or damages resulting from any cause(s) over which DocLink or they do not have direct control, including, but not limited to, failure of electronic or mechanical equipment or communication lines, telephone or other interconnect problems, computer viruses or other damaging code or data, unauthorized access, theft, operator errors, severe weather, earthquakes, natural disasters, strikes or other labour problems, wars, or governmental restrictions.</h6>
   
   <h1>General Provisions</h1>
   
   <h6>This Agreement contains the entire agreement and understanding of the parties concerning the subject matter hereof and supersedes all prior written and oral understandings of the parties concerning the subject matter thereof. You may not assign or sub-license any of the rights and obligations under this Agreement without the prior written consent of DocLink. Any notices in this regard need to be delivered in written format and acknowledged as received. DocLink may subcontract its responsibilities under this Agreement, without your consent to a third party considered by DocLink in good faith to be of equal standing and integrity provided that material provisions of this Agreement shall be reflected in any agreement entered into between DocLink and such third party. No partnership, joint venture, agency or employment relationship is created as a result of these Terms of Use, and neither party has any authority of any kind to bind the other in any respect. If for any reason any provision of this Agreement is held invalid or otherwise unenforceable, such invalidity or unenforceability shall not affect the remainder of this Agreement, and this Agreement shall continue in full force and effect to the fullest extent allowed by law. The parties knowingly and expressly consent to the foregoing terms and conditions.</h6>
</div>

              </div> 
            </div>
          </div>
          </div>
        </div>
        
    )
  } 
}


export default PrivacyModal;