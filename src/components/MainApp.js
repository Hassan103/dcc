import React, { Component, Fragment  } from 'react';
import Home from './Home'
import AboutUs from './AboutUs'
import PrivacyPolicy from './PrivacyPolicy'
import RefundPolicy from './RefundPolicy'
import TermsConditions from './TermsConditions'


class MainApp extends Component{
    constructor(props){
        super(props);
        this.state = {
            isActive : 'home'
        }
    }

    activeScreen = () => {
        console.log("Testing...")
    }

    render(){
        return(
            <Fragment>
                <Home /> 
                <AboutUs />
                <PrivacyPolicy />
                <RefundPolicy />
                <TermsConditions />
            </Fragment>
        )
    }
}

export default MainApp;